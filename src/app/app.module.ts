import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { ErrorInterceptor } from './shared/interceptors/error.interceptor';
import { API_URL } from './shared/utils';

const appRoutes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'pizzas' },
  {
    path: 'pizzas',
    loadChildren: () => import('./pizzas/pizzas.module').then(m => m.PizzasModule)
  }
];
@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  declarations: [AppComponent],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: API_URL,
      useValue: 'http://localhost:3000'
    },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ]
})
export class AppModule {}
