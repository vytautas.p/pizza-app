import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerComponent implements OnChanges {
  @Input() diameter = 40;

  margin = this.getBorderAndMargin(this.diameter);

  ngOnChanges({ diameter }: SimpleChanges): void {
    if (diameter?.currentValue) {
      this.margin = this.getBorderAndMargin(diameter?.currentValue);
    }
  }

  getBorderAndMargin(diameter: number): number {
    return diameter * 0.125;
  }
}
