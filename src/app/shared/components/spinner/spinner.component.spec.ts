import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpinnerComponent } from './spinner.component';

describe('SpinnerComponent', () => {
  let component: SpinnerComponent;
  let fixture: ComponentFixture<SpinnerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpinnerComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SpinnerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnChanges', () => {
    it('should update margin value', () => {
      const diameter = 10;
      const diameterChange = new SimpleChange(undefined, diameter, true)
      const getBorderAndMarginSpy = spyOn(component, 'getBorderAndMargin');

      component.ngOnChanges({diameter: diameterChange});

      expect(getBorderAndMarginSpy).toHaveBeenCalledWith(diameter);
    });

    it('should do nothing', () => {
      const getBorderAndMarginSpy = spyOn(component, 'getBorderAndMargin');

      component.ngOnChanges({});

      expect(getBorderAndMarginSpy).not.toHaveBeenCalled();
    });
  });

  describe('getBorderAndMargin', () => {
    it('should return 1/8 of numeric value', () => {
      const value = 1;

      expect(component.getBorderAndMargin(value)).toEqual(value * 0.125);
    });
  });
});
