import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  // eslint-disable-next-line @angular-eslint/component-selector
  selector: 'button[app-button], a[app-button]',
  template: '<ng-content></ng-content>',
  changeDetection: ChangeDetectionStrategy.OnPush,
  // eslint-disable-next-line @angular-eslint/no-host-metadata-property
  host: {
    class: 'btn',
    ['[class.btn_green]']: 'color === "green"',
    ['[class.btn_red]']: 'color === "red"'
  }
})
export class ButtonComponent {
  @Input() color: 'green' | 'red' = 'green';
}
