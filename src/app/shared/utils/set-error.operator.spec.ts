import { fakeAsync, tick } from '@angular/core/testing';
import { of, throwError } from 'rxjs';

import { setError } from './set-error.operator';

describe(`setError Operator`, () => {
  it(`should reset error`, fakeAsync(() => {
    const response = null;
    const errorCallbackMock = jasmine.createSpy('errorCallbackMock');

    of(response).pipe(setError(errorCallbackMock)).subscribe();
    tick();

    expect(errorCallbackMock).toHaveBeenCalledWith(null);
  }));

  it(`should set error and rethrow it`, fakeAsync(() => {
    const error = 'error';
    const errorCallbackMock = jasmine.createSpy('errorCallbackMock');

    throwError(() => error)
      .pipe(setError(errorCallbackMock))
      .subscribe({
        next: () => fail('Map error should have failed!'),
        error: err => expect(err).toEqual(error)
      });
    tick();

    expect(errorCallbackMock).toHaveBeenCalledWith(error);
  }));
});
