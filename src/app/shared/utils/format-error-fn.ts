import { HttpErrorResponse } from '@angular/common/http';

import { ErrorMessage } from '../models';

export function formatErrorFn(originalError: HttpErrorResponse): ErrorMessage {
  return {
    ...originalError,
    formattedMessage: `${originalError.status} ${originalError.statusText}`
  };
}
