import { HttpErrorResponse } from "@angular/common/http";
import { formatErrorFn } from "./format-error-fn";

describe('formatErrorFn', () => {
  it('should add formattedMessage', () => {
    const status = 401;
    const statusText = "Unauthorized";
    const originalError = new HttpErrorResponse({
      status,
      statusText
    });

    const formattedError = formatErrorFn(originalError);

    expect(formattedError?.formattedMessage).toEqual(`${originalError.status} ${originalError.statusText}`)
  });
});
