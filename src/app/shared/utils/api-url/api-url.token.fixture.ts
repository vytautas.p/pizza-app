import { Provider } from "@angular/core";
import { API_URL } from "./api-url.token";

export function provideApiUrl(apiUrl?: string): Provider {
    return {
        provide: API_URL,
        useValue: apiUrl || 'apiUrl'
    }
}