import { catchError, defer, MonoTypeOperatorFunction, Observable, throwError } from 'rxjs';

export function setError<T>(callback: (error: any | null) => void): MonoTypeOperatorFunction<T> {
  return function <T>(source: Observable<T>): Observable<T> {
    return defer(() => {
      callback(null);

      return source.pipe(
        catchError(error => {
          callback(error);
          return throwError(() => error);
        })
      );
    });
  };
}
