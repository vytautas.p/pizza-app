import { fakeAsync, tick } from '@angular/core/testing';
import { of } from 'rxjs';
import { delay } from 'rxjs/operators';

import { setLoading } from './set-loading.operator';


describe(`SetLoading Operator`, () => {
  it(`should set loading to true and to false on complete`, fakeAsync(() => {
    const loadingCallbackMock = jasmine.createSpy('loadingCallbackMock');

    of(null).pipe(delay(1000), setLoading(loadingCallbackMock)).subscribe();
    tick(500);

    expect(loadingCallbackMock).toHaveBeenCalledWith(true);

    tick(600);
    expect(loadingCallbackMock).toHaveBeenCalledWith(false);
  }));
});
