import { defer, finalize, MonoTypeOperatorFunction, Observable } from 'rxjs';

export function setLoading<T>(callback: (loading: boolean) => void): MonoTypeOperatorFunction<T> {
  return function <T>(source: Observable<T>): Observable<T> {
    return defer(() => {
      callback(true);

      return source.pipe(
        finalize(() => {
          callback(false);
        })
      );
    });
  };
}
