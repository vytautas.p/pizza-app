import { HttpErrorResponse } from '@angular/common/http';

export type ErrorMessage = HttpErrorResponse & {
  formattedMessage: string;
} | null
