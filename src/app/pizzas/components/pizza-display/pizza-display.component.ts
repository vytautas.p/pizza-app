import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Topping } from '../../models';
import { DROP_ANIMATION } from '../../../shared/animations';

@Component({
  selector: 'app-pizza-display',
  templateUrl: './pizza-display.component.html',
  styleUrls: ['./pizza-display.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [DROP_ANIMATION]
})
export class PizzaDisplayComponent {
  @Input() toppings!: Topping[] | null;

  trackByFn(index: number, topping: Topping): number {
    return topping.id;
  }
}
