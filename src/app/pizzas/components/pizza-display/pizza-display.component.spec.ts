import { ComponentFixture, TestBed } from '@angular/core/testing';
import { toppingFixture } from '../../fixtures';

import { PizzaDisplayComponent } from './pizza-display.component';

describe('PizzaDisplayComponent', () => {
  let component: PizzaDisplayComponent;
  let fixture: ComponentFixture<PizzaDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PizzaDisplayComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(PizzaDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('trackByFn', () => {
    it('should return topping id', () => {
      const index = 0;
      const topping = toppingFixture;

      expect(component.trackByFn(index, topping)).toEqual(topping.id);
    });
  });
});
