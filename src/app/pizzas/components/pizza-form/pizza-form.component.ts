import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';

import { NewPizza, Pizza, Topping } from '../../models';

@Component({
  selector: 'app-pizza-form',
  templateUrl: './pizza-form.component.html',
  styleUrls: ['./pizza-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PizzaFormComponent implements OnChanges, OnInit, OnDestroy {
  @Input() pizza!: Pizza | null;
  @Input() toppings!: Topping[] | null;
  @Output() selected = new EventEmitter<Topping[]>();
  @Output() create = new EventEmitter<NewPizza>();
  @Output() update = new EventEmitter<Pizza>();
  @Output() remove = new EventEmitter<void>();

  form = this.fb.group({
    name: this.fb.nonNullable.control('', Validators.required),
    toppings: this.fb.nonNullable.control<Topping[]>([])
  });
  subscriptions = new Subscription();

  constructor(private fb: FormBuilder) {}

  get nameControl(): FormControl<string> {
    return this.form.controls.name;
  }

  get nameControlInvalid(): boolean {
    return this.nameControl.hasError('required') && this.nameControl.touched;
  }

  ngOnChanges({ pizza }: SimpleChanges): void {
    if (pizza?.currentValue) {
      const { name, toppings } = pizza.currentValue as Pizza;

      this.form.patchValue({ name, toppings });
    }
  }

  ngOnInit(): void {
    this.subscriptions.add(
      this.form.controls.toppings.valueChanges.subscribe(value => this.selected.emit(value))
    );
  }

  createPizza(): void {
    const { valid, touched } = this.form;

    if (touched && valid) {
      this.create.emit(this.form.getRawValue());
    }
  }

  updatePizza(pizza: Pizza): void {
    const { valid, touched } = this.form;

    if (touched && valid) {
      this.update.emit({ ...pizza, ...this.form.getRawValue() });
    }
  }

  removePizza() {
    this.remove.emit();
  }

  ngOnDestroy(): void {
    this.subscriptions.unsubscribe();
  }
}
