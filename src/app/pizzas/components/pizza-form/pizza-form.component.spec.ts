import { SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { MockComponents } from 'ng-mocks';
import { Subscription } from 'rxjs';

import { pizzaFixture, toppingFixture } from '../../fixtures';
import { newPizzaFixture } from '../../fixtures/new-pizza.fixture';
import { PizzaToppingsComponent } from '../pizza-toppings/pizza-toppings.component';
import { PizzaFormComponent } from './pizza-form.component';

describe('PizzaFormComponent', () => {
  let component: PizzaFormComponent;
  let fixture: ComponentFixture<PizzaFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ReactiveFormsModule],
      declarations: [PizzaFormComponent, MockComponents(PizzaToppingsComponent)]
    }).compileComponents();

    fixture = TestBed.createComponent(PizzaFormComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('get nameControl', () => {
    it('should return name control from the form group', () => {
      expect(component.nameControl).toBe(component.form.controls.name);
    });
  });

  describe('get nameControlInvalid', () => {
    [
      {
        touched: false,
        errors: {},
        output: false,
        description: 'should return false when control was not touched and error is not set'
      },
      {
        touched: true,
        errors: {},
        output: false,
        description: 'should return false when control was touched and error is not set'
      },
      {
        touched: false,
        errors: { required: true },
        output: false,
        description: 'should return false when control was not touched and error is set to required'
      },
      {
        touched: true,
        errors: { required: true },
        output: true,
        description: 'should return true when control was touched and error is set to required'
      }
    ].forEach(({ touched, errors, output, description }) => {
      it(description, () => {
        const nameControl = component.nameControl;

        touched ? nameControl.markAsTouched() : component.nameControl.markAsUntouched();
        nameControl.setErrors(errors);

        expect(component.nameControlInvalid).toEqual(output);
      });
    });
  });

  describe('ngOnChanges', () => {
    it('should patch form values from the current pizza', () => {
      const pizza = pizzaFixture;
      const pizzaChange = new SimpleChange(undefined, pizza, false);
      const patchValueSpy = spyOn(component.form, 'patchValue');

      component.ngOnChanges({ pizza: pizzaChange });

      expect(patchValueSpy).toHaveBeenCalledWith({ name: pizza.name, toppings: pizza.toppings });
    });

    it('should do nothing', () => {
      const patchValueSpy = spyOn(component.form, 'patchValue');

      component.ngOnChanges({});

      expect(patchValueSpy).not.toHaveBeenCalled();
    });
  });

  describe('ngOnInit', () => {
    it('should add subscription to the Subscription instance', () => {
      const subscription = new Subscription();
      const addSpy = spyOn(component.subscriptions, 'add');

      spyOn(component.form.controls.toppings.valueChanges, 'subscribe').and.returnValue(
        subscription
      );
      component.ngOnInit();

      expect(addSpy).toHaveBeenCalledWith(subscription);
    });
  });

  describe('toppings.valueChanges', () => {
    it('should emit selected event on change to toppings control', () => {
      const selectedSpy = spyOn(component.selected, 'emit');
      const toppings = [toppingFixture];

      component.form.controls.toppings.setValue(toppings);

      expect(selectedSpy).toHaveBeenCalledWith(toppings);
    });
  });

  describe('createPizza', () => {
    [
      {
        touched: false,
        valid: false,
        description: 'should not emit create event when form is untouched and invalid'
      },
      {
        touched: true,
        valid: false,
        description: 'should not emit create event when form is touched and invalid'
      },
      {
        touched: false,
        valid: true,
        description: 'should not emit create event when form is untouched and valid'
      }
    ].forEach(({ touched, valid, description }) => {
      it(description, () => {
        const createSpy = spyOn(component.create, 'emit');

        spyOnProperty(component.form, 'valid').and.returnValue(valid);
        touched ? component.form.markAsTouched() : component.form.markAsUntouched();
        component.createPizza();

        expect(createSpy).not.toHaveBeenCalled();
      });
    });

    it('should emit create event with raw form value', () => {
      const newPizza = newPizzaFixture;
      const createSpy = spyOn(component.create, 'emit');

      spyOn(component.form, 'getRawValue').and.returnValue(newPizza);
      spyOnProperty(component.form, 'valid').and.returnValue(true);
      component.form.markAsTouched();
      component.createPizza();

      expect(createSpy).toHaveBeenCalledWith(newPizzaFixture);
    });
  });

  describe('updatePizza', () => {
    [
      {
        touched: false,
        valid: false,
        description: 'should not emit update event when form is untouched and invalid'
      },
      {
        touched: true,
        valid: false,
        description: 'should not emit update event when form is touched and invalid'
      },
      {
        touched: false,
        valid: true,
        description: 'should not emit update event when form is untouched and valid'
      }
    ].forEach(({ touched, valid, description }) => {
      it(description, () => {
        const pizza = pizzaFixture;
        const updateSpy = spyOn(component.update, 'emit');

        spyOnProperty(component.form, 'valid').and.returnValue(valid);
        touched ? component.form.markAsTouched() : component.form.markAsUntouched();
        component.updatePizza(pizza);

        expect(updateSpy).not.toHaveBeenCalled();
      });
    });

    it('should emit update event with raw form value merged with selected pizza', () => {
      const newPizza = newPizzaFixture;
      const pizza = { ...pizzaFixture, name: 'Just the base', toppings: [] };
      const updateSpy = spyOn(component.update, 'emit');

      spyOn(component.form, 'getRawValue').and.returnValue(newPizza);
      spyOnProperty(component.form, 'valid').and.returnValue(true);
      component.form.markAsTouched();
      component.updatePizza(pizza);

      expect(updateSpy).toHaveBeenCalledWith({ ...pizza, ...newPizza });
    });
  });

  describe('removePizza', () => {
    it('should emit remove event', () => {
      const removeSpy = spyOn(component.remove, 'emit');

      component.removePizza();

      expect(removeSpy).toHaveBeenCalled();
    });
  });

  describe('ngOnDestroy', () => {
    it('should unsubscribe main Subscription instance', () => {
      const unsubscribeSpy = spyOn(component.subscriptions, 'unsubscribe').and.callThrough();

      component.ngOnDestroy();

      expect(unsubscribeSpy).toHaveBeenCalled();
    });
  });
});
