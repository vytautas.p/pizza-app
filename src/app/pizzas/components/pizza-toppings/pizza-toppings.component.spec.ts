import { ChangeDetectorRef, SimpleChange } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NG_VALUE_ACCESSOR } from '@angular/forms';

import { toppingFixture } from '../../fixtures';
import { PizzaToppingsComponent } from './pizza-toppings.component';

describe('PizzaToppingsComponent', () => {
  let component: PizzaToppingsComponent;
  let fixture: ComponentFixture<PizzaToppingsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PizzaToppingsComponent]
    }).compileComponents();

    fixture = TestBed.createComponent(PizzaToppingsComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('NG_VALUE_ACCESSOR', () => {
    it('should return component declaration', () => {
      const [ngValueAccessor] = fixture.debugElement.injector.get(NG_VALUE_ACCESSOR);

      expect(ngValueAccessor instanceof PizzaToppingsComponent).toBeTrue();
    });
  });

  describe('ngOnChanges', () => {
    it('should not override non-null values', () => {
      const toppings = [toppingFixture];
      const toppingsChange = new SimpleChange(undefined, toppings, true);

      component.toppings = toppings;
      component.ngOnChanges({ toppings: toppingsChange });

      expect(component.toppings).toEqual(toppings);
    });

    it('should override null values within an empty array', () => {
      const toppings = null;
      const toppingsChange = new SimpleChange(undefined, toppings, true);

      component.ngOnChanges({ toppings: toppingsChange });

      expect(component.toppings).toEqual([]);
    });

    it('should do nothing', () => {
      const prevToppings = component.toppings;

      component.ngOnChanges({});

      expect(component.toppings).toBe(prevToppings);
    });
  });

  describe('registerOnChange', () => {
    it('should register ControlValueAccessor onChange callback function', () => {
      const onChangeFn = jasmine.createSpy('onChangeFn');

      component.registerOnChange(onChangeFn);

      expect(component.onModelChange).toBe(onChangeFn);
    });
  });

  describe('registerOnTouched', () => {
    it('should register ControlValueAccessor onTouched callback function', () => {
      const onTouchedFn = jasmine.createSpy('onTouchedFn');

      component.registerOnTouched(onTouchedFn);

      expect(component.onTouch).toBe(onTouchedFn);
    });
  });

  describe('writeValue', () => {
    it('should write new value to the control', () => {
      const toppings = [toppingFixture];
      const cdRef = fixture.debugElement.injector.get(ChangeDetectorRef);
      const markForCheckSpy = spyOn(cdRef.constructor.prototype, 'markForCheck');

      component.writeValue(toppings);

      expect(component.value).toEqual(toppings);
      expect(markForCheckSpy).toHaveBeenCalled();
    });
  });

  describe('selectTopping', () => {
    it('should append selected topping to the value and mark control as touched and trigger value change', () => {
      const topping = toppingFixture;
      const prevValue = component.value;
      const existsInToppingsSpy = spyOn(component, 'existsInToppings');
      const onTouchSpy = jasmine.createSpy('onTouchSpy');
      const onModelChangeSpy = jasmine.createSpy('onModelChangeSpy');

      component.onTouch = onTouchSpy;
      component.onModelChange = onModelChangeSpy;
      existsInToppingsSpy.and.returnValue(false);
      component.selectTopping(topping);

      expect(component.value).toEqual([...prevValue, topping]);
      expect(component.onTouch).toHaveBeenCalled();
      expect(component.onModelChange).toHaveBeenCalledWith(component.value);
    });

    it('should filter out selected topping from the value and mark control as touched and trigger value change', () => {
      const topping = toppingFixture;
      const existsInToppingsSpy = spyOn(component, 'existsInToppings');
      const onTouchSpy = jasmine.createSpy('onTouchSpy');
      const onModelChangeSpy = jasmine.createSpy('onModelChangeSpy');

      component.onTouch = onTouchSpy;
      component.onModelChange = onModelChangeSpy;
      existsInToppingsSpy.and.returnValue(true);
      component.value = [topping];
      component.selectTopping(topping);

      expect(component.value).toEqual([]);
      expect(component.onTouch).toHaveBeenCalled();
      expect(component.onModelChange).toHaveBeenCalledWith(component.value);
    });
  });

  describe('existsInToppings', () => {
    it('should return true if there is at least one topping id match within control value', () => {
      const topping = toppingFixture;

      component.value = [topping];

      expect(component.existsInToppings(topping)).toBeTrue();
    });

    it('should return false if there is no topping id match within control value', () => {
      const topping = { ...toppingFixture, id: 1 };

      component.value = [{ ...topping, id: 2 }];

      expect(component.existsInToppings(topping)).toBeFalse();
    });
  });
});
