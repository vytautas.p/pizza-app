import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { Topping } from '../../models';

@Component({
  selector: 'app-pizza-toppings',
  templateUrl: './pizza-toppings.component.html',
  styleUrls: ['./pizza-toppings.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PizzaToppingsComponent),
      multi: true
    }
  ]
})
export class PizzaToppingsComponent implements ControlValueAccessor, OnChanges {
  @Input() toppings!: Topping[] | null;

  value: Topping[] = [];

  onTouch!: () => void;
  onModelChange!: (toppings: Topping[]) => void;

  constructor(private cdRef: ChangeDetectorRef) {}

  ngOnChanges({ toppings }: SimpleChanges): void {
    if (toppings?.currentValue === null) {
      this.toppings = toppings.currentValue || [];
    }
  }

  registerOnChange(fn: typeof this.onModelChange): void {
    this.onModelChange = fn;
  }

  registerOnTouched(fn: typeof this.onTouch): void {
    this.onTouch = fn;
  }

  writeValue(value: Topping[]): void {
    this.value = value;
    this.cdRef.markForCheck();
  }

  selectTopping(topping: Topping): void {
    if (this.existsInToppings(topping)) {
      this.value = this.value.filter(item => item.id !== topping.id);
    } else {
      this.value = [...this.value, topping];
    }

    this.onTouch();
    this.onModelChange(this.value);
  }

  existsInToppings(topping: Topping): boolean {
    return this.value.some(val => val.id === topping.id);
  }
}
