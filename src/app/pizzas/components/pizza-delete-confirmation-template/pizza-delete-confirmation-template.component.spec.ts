import { ComponentFixture, TestBed } from '@angular/core/testing';
import { pizzaFixture } from '../../fixtures';

import { PizzaDeleteConfirmationTemplateComponent } from './pizza-delete-confirmation-template.component';

describe('PizzaDeleteConfirmationTemplateComponent', () => {
  let component: PizzaDeleteConfirmationTemplateComponent;
  let fixture: ComponentFixture<PizzaDeleteConfirmationTemplateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PizzaDeleteConfirmationTemplateComponent ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(PizzaDeleteConfirmationTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('onCancel', () => {
    it('should emit cancel event', () => {
      const emitSpy = spyOn(component.cancel, 'emit');

      component.onCancel();

      expect(emitSpy).toHaveBeenCalled();
    })
  });

  describe('onDelete', () => {
    it('should emit delete event with provided pizza', () => {
      const pizza = pizzaFixture;
      const emitSpy = spyOn(component.delete, 'emit');

      component.onDelete(pizza);

      expect(emitSpy).toHaveBeenCalled();
    })
  })
});
