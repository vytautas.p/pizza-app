import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';
import { Pizza } from '../../models';

@Component({
  selector: 'app-pizza-delete-confirmation-template',
  templateUrl: './pizza-delete-confirmation-template.component.html',
  styleUrls: ['./pizza-delete-confirmation-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PizzaDeleteConfirmationTemplateComponent {
  @Input() pizza!: Pizza | null;
  @Output() cancel = new EventEmitter<void>();
  @Output() delete = new EventEmitter<Pizza>();

  onCancel(): void {
    this.cancel.emit();
  }

  onDelete(pizza: Pizza): void {
    this.delete.emit(pizza);
  }
}
