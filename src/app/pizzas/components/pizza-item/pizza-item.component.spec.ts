import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { MockComponents } from 'ng-mocks';

import { PizzaItemComponent } from './pizza-item.component';
import { PizzaDisplayComponent } from '../pizza-display/pizza-display.component';
import { pizzaFixture } from '../../fixtures';

describe('PizzaItemComponent', () => {
  let component: PizzaItemComponent;
  let fixture: ComponentFixture<PizzaItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [PizzaItemComponent, MockComponents(PizzaDisplayComponent)]
    }).compileComponents();

    fixture = TestBed.createComponent(PizzaItemComponent);
    component = fixture.componentInstance;

    component.pizza = pizzaFixture;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
