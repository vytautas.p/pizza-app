import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { API_URL } from '../../shared/utils';
import { NewPizza, Pizza } from '../models';

@Injectable()
export class PizzasService {
  url = `${inject(API_URL)}/pizzas`;

  constructor(private http: HttpClient) {}

  getPizzas(): Observable<Pizza[]> {
    return this.http.get<Pizza[]>(`${this.url}`);
  }

  getPizzaById(id: string | number): Observable<Pizza> {
    return this.http.get<Pizza>(`${this.url}/${id}`);
  }

  createPizza(newPizza: NewPizza): Observable<Pizza> {
    return this.http.post<Pizza>(`${this.url}`, newPizza);
  }

  updatePizza(pizza: Pizza): Observable<Pizza> {
    return this.http.put<Pizza>(`${this.url}/${pizza.id}`, pizza);
  }

  removePizza(pizza: Pizza): Observable<Record<string, never>> {
    return this.http.delete<Record<string, never>>(`${this.url}/${pizza.id}`);
  }
}
