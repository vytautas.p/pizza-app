import { HttpClient } from '@angular/common/http';
import { inject, Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { API_URL } from '../../shared/utils';
import { Topping } from '../models';

@Injectable()
export class ToppingsService {
  url = `${inject(API_URL)}/toppings`;

  constructor(private http: HttpClient) {}

  getToppings(): Observable<Topping[]> {
    return this.http.get<Topping[]>(`${this.url}`);
  }
}
