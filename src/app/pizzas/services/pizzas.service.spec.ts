import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { PizzasService } from './pizzas.service';
import { provideApiUrl } from '../../shared/utils';
import { pizzaFixture } from '../fixtures';

describe('PizzasService', () => {
  let service: PizzasService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [PizzasService, provideApiUrl()]
    });

    service = TestBed.inject(PizzasService);
    http = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getPizzas', () => {
    it('should fetch available pizzas', () => {
      const response = [pizzaFixture];

      service.getPizzas().subscribe({
        next: res => expect(res).toEqual(response),
        error: () => fail('failed to test getPizzas')
      });

      const req = http.expectOne(`${service.url}`);

      expect(req.request.method).toEqual('GET');
      req.flush(response);
    });
  });

  describe('getPizzaById', () => {
    it('should fetch pizza by id', () => {
      const response = pizzaFixture;
      const { id } = response;

      service.getPizzaById(id).subscribe({
        next: res => expect(res).toEqual(response),
        error: () => fail('failed to test getPizzaById')
      });

      const req = http.expectOne(`${service.url}/${id}`);

      expect(req.request.method).toEqual('GET');
      req.flush(response);
    });
  });

  describe('createPizza', () => {
    it('should post provided pizza', () => {
      const response = pizzaFixture;
      const { id, ...pizza } = response;

      service.createPizza(pizza).subscribe({
        next: res => expect(res).toEqual(response),
        error: () => fail('failed to test createPizza')
      });

      const req = http.expectOne(`${service.url}`);

      expect(req.request.method).toEqual('POST');
      expect(req.request.body).toEqual(pizza);
      req.flush(response);
    });
  });

  describe('updatePizza', () => {
    it('should post provided pizza', () => {
      const response = pizzaFixture;
      const { id } = response;

      service.updatePizza(response).subscribe({
        next: res => expect(res).toEqual(response),
        error: () => fail('failed to test updatePizza')
      });

      const req = http.expectOne(`${service.url}/${id}`);

      expect(req.request.method).toEqual('PUT');
      expect(req.request.body).toEqual(response);
      req.flush(response);
    });
  });

  describe('removePizza', () => {
    it('should fetch pizza by id', () => {
      const pizza= pizzaFixture;
      const response = {} as Record<string, never>;

      service.removePizza(pizza).subscribe({
        next: res => expect(res).toEqual(response),
        error: () => fail('failed to test removePizza')
      });

      const req = http.expectOne(`${service.url}/${pizza.id}`);

      expect(req.request.method).toEqual('DELETE');
      req.flush(response);
    });
  });
});
