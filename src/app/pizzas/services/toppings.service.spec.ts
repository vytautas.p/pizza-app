import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';

import { provideApiUrl } from '../../shared/utils';
import { toppingFixture } from '../fixtures';
import { ToppingsService } from './toppings.service';

describe('ToppingsService', () => {
  let service: ToppingsService;
  let http: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ToppingsService, provideApiUrl()]
    });
    service = TestBed.inject(ToppingsService);
    http = TestBed.inject(HttpTestingController)
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  describe('getToppings', () => {
    it('should fetch available toppings', () => {
      const response = [toppingFixture];

      service.getToppings().subscribe({
        next: res => expect(res).toEqual(response),
        error: () => fail('failed to test getToppings')
      });

      const req = http.expectOne(`${service.url}`);

      expect(req.request.method).toEqual('GET');
      req.flush(response);
    });
  });
});
