import { Params } from '@angular/router';

export function selectPizzaIdParamFn(params: Params): number {
  return parseInt(params['pizzaId']);
}
