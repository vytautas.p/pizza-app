import { Topping } from '../models';

export const toppingFixture: Topping = {
  id: 1,
  name: 'mozzarella'
};
