import { Pizza } from '../models';
import { newPizzaFixture } from './new-pizza.fixture';

export const pizzaFixture: Pizza = {
  ...newPizzaFixture,
  id: 1
};
