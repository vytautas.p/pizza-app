import { NewPizza } from '../models';
import { toppingFixture } from './topping.fixture';

export const newPizzaFixture: NewPizza = {
  name: 'Margarita',
  toppings: [toppingFixture]
};
