import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { DialogModule } from '@angular/cdk/dialog';

import { ButtonModule } from '../shared/components/button/button.module';
import { SpinnerModule } from '../shared/components/spinner/spinner.module';

import { PizzasService } from './services/pizzas.service';
import { ToppingsService } from './services/toppings.service';
import { PizzaListComponent } from './containers/pizza-list/pizza-list.component';
import { PizzaDetailComponent } from './containers/pizza-detail/pizza-detail.component';
import { PizzaItemComponent } from './components/pizza-item/pizza-item.component';
import { PizzaDisplayComponent } from './components/pizza-display/pizza-display.component';
import { PizzaFormComponent } from './components/pizza-form/pizza-form.component';
import { PizzaToppingsComponent } from './components/pizza-toppings/pizza-toppings.component';
import { PizzaDeleteConfirmationComponent } from './containers/pizza-delete-confirmation/pizza-delete-confirmation.component';
import { PizzaDeleteConfirmationTemplateComponent } from './components/pizza-delete-confirmation-template/pizza-delete-confirmation-template.component';
import { PizzaResolver } from './resolvers/pizza.resolver';

const pizzaRoutes: Routes = [
  { path: '', component: PizzaListComponent },
  {
    path: 'new',
    component: PizzaDetailComponent
  },
  {
    path: ':pizzaId',
    component: PizzaDetailComponent,
    resolve: {
      pizza: PizzaResolver
    },
    children: [
      {
        path: 'delete',
        component: PizzaDeleteConfirmationComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(pizzaRoutes),
    DialogModule,
    ButtonModule,
    SpinnerModule
  ],
  providers: [PizzasService, ToppingsService, PizzaResolver],
  declarations: [
    PizzaListComponent,
    PizzaDetailComponent,
    PizzaItemComponent,
    PizzaDisplayComponent,
    PizzaFormComponent,
    PizzaToppingsComponent,
    PizzaDeleteConfirmationComponent,
    PizzaDeleteConfirmationTemplateComponent
  ]
})
export class PizzasModule {}
