import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot } from '@angular/router';
import {  Observable, of } from 'rxjs';

import { Pizza } from '../models';
import { PizzasService } from '../services/pizzas.service';
import { selectPizzaIdParamFn } from '../utils';

@Injectable()
export class PizzaResolver implements Resolve<Pizza | null> {
  constructor(private pizzasService: PizzasService) {}

  resolve(route: ActivatedRouteSnapshot): Observable<Pizza | null> {
    const id = selectPizzaIdParamFn(route.params);

    if (!id) {
      return of(null);
    }

    return this.pizzasService.getPizzaById(id);
  }
}
