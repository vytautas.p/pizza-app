import { TestBed } from '@angular/core/testing';
import { ActivatedRouteSnapshot, Params } from '@angular/router';
import { of } from 'rxjs';

import { pizzaFixture } from '../fixtures';
import { PizzasService } from '../services/pizzas.service';
import { PizzaResolver } from './pizza.resolver';

describe('PizzaResolver', () => {
  let resolver: PizzaResolver;
  let pizzaService: jasmine.SpyObj<PizzasService>;

  beforeEach(() => {
    pizzaService = jasmine.createSpyObj('PizzaService', ['getPizzaById']);
  });

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [PizzaResolver, { provide: PizzasService, useValue: pizzaService }]
    });
    resolver = TestBed.inject(PizzaResolver);
  });

  it('should be created', () => {
    expect(resolver).toBeTruthy();
  });

  describe('resolve', () => {
    it('should return null', () => {
      const params: Params = {};
      const snapshot = { ...new ActivatedRouteSnapshot(), params } as ActivatedRouteSnapshot;

      resolver.resolve(snapshot).subscribe({
        next: res => expect(res).toBeNull(),
        error: () => fail('failed to test pizza resolver')
      });
    });

    it('should return pizza by the id present within route snapshot', () => {
      const pizzaId = 1;
      const pizza = pizzaFixture;
      const params: Params = { pizzaId };
      const snapshot = { ...new ActivatedRouteSnapshot(), params } as ActivatedRouteSnapshot;

      pizzaService.getPizzaById.and.returnValue(of(pizza));
      resolver.resolve(snapshot).subscribe({
        next: res => expect(res).toEqual(pizza),
        error: () => fail('failed to test pizza resolver')
      });

      expect(pizzaService.getPizzaById).toHaveBeenCalledWith(pizzaId);
    });
  });
});
