import { ComponentFixture, fakeAsync, flush, TestBed, tick } from '@angular/core/testing';
import { MockComponents } from 'ng-mocks';
import { EMPTY, Subject } from 'rxjs';

import { PizzaListComponent } from './pizza-list.component';
import { PizzasService } from '../../services/pizzas.service';
import { PizzaItemComponent } from '../../components/pizza-item/pizza-item.component';
import { pizzaFixture } from '../../fixtures';
import { Pizza } from '../../models';

describe('PizzaListComponent', () => {
  let component: PizzaListComponent;
  let fixture: ComponentFixture<PizzaListComponent>;
  let pizzasService: jasmine.SpyObj<PizzasService>;

  beforeEach(() => {
    pizzasService = jasmine.createSpyObj('PizzasService', {
      getPizzas: EMPTY
    });
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [PizzaListComponent, MockComponents(PizzaItemComponent)],
      providers: [
        {
          provide: PizzasService,
          useValue: pizzasService
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PizzaListComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
