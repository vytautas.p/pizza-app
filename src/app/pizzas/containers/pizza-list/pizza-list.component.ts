import { ChangeDetectionStrategy, Component } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { PizzasService } from '../../services/pizzas.service';
import { setError } from '../../../shared/utils/set-error.operator';
import { setLoading } from '../../../shared/utils/set-loading.operator';
import { ErrorMessage } from '../../../shared/models';

@Component({
  selector: 'app-pizza-list',
  templateUrl: './pizza-list.component.html',
  styleUrls: ['./pizza-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PizzaListComponent {
  loading$ = new BehaviorSubject<boolean>(false);
  error$ = new BehaviorSubject<string | null>(null);
  pizzas$ = this.pizzasService.getPizzas().pipe(
    setLoading(loading => this.loading$.next(loading)),
    setError((error: ErrorMessage) => this.error$.next(error?.formattedMessage || null))
  );

  constructor(private pizzasService: PizzasService) {}
}
