import { ChangeDetectionStrategy, Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Dialog, DialogRef } from '@angular/cdk/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import { map, Observable } from 'rxjs';

import { PizzasService } from '../../services/pizzas.service';
import { Pizza } from '../../models';

@Component({
  selector: 'app-pizza-delete-confirmation',
  templateUrl: './pizza-delete-confirmation.component.html',
  styleUrls: ['./pizza-delete-confirmation.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PizzaDeleteConfirmationComponent implements OnInit {
  pizza$ = this.getCurrentPizza();
  dialogRef!: DialogRef<boolean, HTMLElement>;

  @ViewChild('pizzaDeleteConfirmationTemplate', { static: true })
  pizzaDeleteConfirmationTemplate!: TemplateRef<HTMLElement>;

  constructor(
    private dialog: Dialog,
    private router: Router,
    private route: ActivatedRoute,
    private pizzaService: PizzasService
  ) {}

  ngOnInit(): void {
    this.dialogRef = this.dialog.open(this.pizzaDeleteConfirmationTemplate, {
      maxWidth: '90vw'
    });

    this.dialogRef.closed.subscribe(deleted => {
      let relativeRoute = '../';

      if (deleted) {
        relativeRoute += relativeRoute;
      }

      this.router.navigate([relativeRoute], { relativeTo: this.route, replaceUrl: true });
    });
  }

  getCurrentPizza(): Observable<Pizza | null> {
    const route = this.route.parent as ActivatedRoute;

    return route.data.pipe(map(data => data['pizza'] || null));
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  onDelete(pizza: Pizza): void {
    this.pizzaService.removePizza(pizza).subscribe(() => this.dialogRef.close(true));
  }
}
