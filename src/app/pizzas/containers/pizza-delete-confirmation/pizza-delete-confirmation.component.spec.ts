import { Dialog, DialogRef } from '@angular/cdk/dialog';
import { ComponentFixture, fakeAsync, flush, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { cold } from 'jasmine-marbles';
import { of, Subject } from 'rxjs';
import { pizzaFixture } from '../../fixtures';

import { PizzasService } from '../../services/pizzas.service';
import { PizzaDeleteConfirmationComponent } from './pizza-delete-confirmation.component';

describe('PizzaDeleteConfirmationComponent', () => {
  let component: PizzaDeleteConfirmationComponent;
  let fixture: ComponentFixture<PizzaDeleteConfirmationComponent>;
  let closedSubject: Subject<boolean | undefined>;
  let dialogRef: jasmine.SpyObj<DialogRef<boolean, HTMLElement>>;
  let dialog: jasmine.SpyObj<Dialog>;
  let pizzaService: jasmine.SpyObj<PizzasService>;
  let route: ActivatedRoute;
  let router: Router;
  let dataSubject: Subject<Data>;

  beforeEach(() => {
    closedSubject = new Subject();
    dialogRef = jasmine.createSpyObj('DialogRef', ['close'], { closed: closedSubject });
    dialog = jasmine.createSpyObj('Dialog', ['open']);
    dialog.open.and.returnValue(dialogRef as DialogRef<unknown, unknown>);
    pizzaService = jasmine.createSpyObj('PizzaService', ['removePizza']);
    dataSubject = new Subject();
    route = {
      ...new ActivatedRoute(),
      parent: { ...new ActivatedRoute(), data: dataSubject.asObservable() } as ActivatedRoute
    } as ActivatedRoute;
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [PizzaDeleteConfirmationComponent],
      providers: [
        {
          provide: Dialog,
          useValue: dialog
        },
        {
          provide: PizzasService,
          useValue: pizzaService
        },
        {
          provide: ActivatedRoute,
          useValue: route
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PizzaDeleteConfirmationComponent);
    component = fixture.componentInstance;

    router = TestBed.inject(Router);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('ngOnInit', () => {
    it('should open a dialog and assign it to dialogRef', () => {
      component.ngOnInit();

      expect(component.dialogRef).toEqual(dialogRef);
    });

    it('should navigate back to parent once deletion was cancelled', () => {
      const navigateSpy = spyOn(router, 'navigate');

      closedSubject.next(undefined);

      expect(navigateSpy).toHaveBeenCalledWith(['../'], { relativeTo: route, replaceUrl: true});
    });

    it('should navigate back to parent once deletion was confirmed', () => {
      const navigateSpy = spyOn(router, 'navigate');

      closedSubject.next(true);

      expect(navigateSpy).toHaveBeenCalledWith(['../../'], { relativeTo: route, replaceUrl: true});
    });
  });

  describe('getCurrentPizza', () => {
    it('return undefined when pizza is not defined in data object', fakeAsync(() => {
      const pizza = null;
      const data = {};

      component.getCurrentPizza().subscribe({
        next: res => expect(res).toEqual(pizza),
        error: () => fail('failed to getCurrentPizza')
      });
      dataSubject.next(data);
      flush();
    }));

    it('return pizza when pizza is defined in data object', fakeAsync(() => {
      const pizza = pizzaFixture;
      const data = { pizza };

      component.getCurrentPizza().subscribe({
        next: res => expect(res).toEqual(pizza),
        error: () => fail('failed to getCurrentPizza')
      });
      dataSubject.next(data);
      flush();
    }));
  });

  describe('onCancel', () => {
    it('should close the dialog', () => {
      component.onCancel();

      expect(dialogRef.close).toHaveBeenCalled();
    });
  });

  describe('onDelete', () => {
    it('should remove pizza and close the dialog', () => {
      const pizza = pizzaFixture;

      pizzaService.removePizza.and.returnValue(of({}));
      component.onDelete(pizza);

      expect(pizzaService.removePizza).toHaveBeenCalled();
      expect(dialogRef.close).toHaveBeenCalledWith(true);
    });
  });
});
