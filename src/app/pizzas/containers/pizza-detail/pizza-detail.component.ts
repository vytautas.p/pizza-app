import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BehaviorSubject, combineLatest, map, Observable } from 'rxjs';

import { NewPizza, Pizza, Topping } from '../../models';
import { PizzasService } from '../../services/pizzas.service';
import { ToppingsService } from '../../services/toppings.service';

@Component({
  selector: 'app-pizza-detail',
  templateUrl: './pizza-detail.component.html',
  styleUrls: ['./pizza-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PizzaDetailComponent {
  selectedToppings$ = new BehaviorSubject<Topping[]>([]);
  pizza$ = this.getCurrentPizza();
  toppings$ = this.getToppings();
  visualise$ = this.getVisualisedToppings();

  constructor(
    private pizzaService: PizzasService,
    private toppingsService: ToppingsService,
    private route: ActivatedRoute,
    private router: Router
  ) {}

  getCurrentPizza(): Observable<Pizza | null> {
    return this.route.data.pipe(map(data => data['pizza'] || null));
  }

  getToppings(): Observable<Topping[]> {
    return this.toppingsService.getToppings();
  }

  getVisualisedToppings(): Observable<Topping[]> {
    return combineLatest([this.selectedToppings$, this.getCurrentPizza()]).pipe(
      map(([selectedToppings, pizza], index) => {
        if (index) {
          return selectedToppings;
        }

        if (!pizza) {
          return [];
        }

        return pizza.toppings;
      })
    );
  }

  onSelect(toppings: Topping[]): void {
    this.selectedToppings$.next(toppings);
  }

  onCreate(newPizza: NewPizza): void {
    this.pizzaService.createPizza(newPizza).subscribe(pizza => {
      this.router.navigate([`/pizzas/${pizza.id}`]);
    });
  }

  onUpdate(pizza: Pizza): void {
    this.pizzaService.updatePizza(pizza).subscribe(() => {
      this.router.navigate(['/pizzas']);
    });
  }

  onRemove(): void {
    this.router.navigate(['delete'], { relativeTo: this.route });
  }
}
