import { ComponentFixture, fakeAsync, flush, TestBed } from '@angular/core/testing';
import { ActivatedRoute, Data, Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { MockComponents } from 'ng-mocks';
import { BehaviorSubject, of, Subject } from 'rxjs';
import {  hot } from 'jasmine-marbles';

import { PizzaDetailComponent } from './pizza-detail.component';
import { PizzasService } from '../../services/pizzas.service';
import { ToppingsService } from '../../services/toppings.service';
import { PizzaDisplayComponent } from '../../components/pizza-display/pizza-display.component';
import { PizzaFormComponent } from '../../components/pizza-form/pizza-form.component';
import { pizzaFixture, toppingFixture } from '../../fixtures';
import { Topping } from '../../models';
import { newPizzaFixture } from '../../fixtures/new-pizza.fixture';

describe('PizzaDetailComponent', () => {
  let component: PizzaDetailComponent;
  let fixture: ComponentFixture<PizzaDetailComponent>;
  let pizzasService: jasmine.SpyObj<PizzasService>;
  let toppingsService: jasmine.SpyObj<ToppingsService>;
  let router: Router;
  let route: ActivatedRoute;
  let dataSubject: Subject<Data>;

  beforeEach(() => {
    pizzasService = jasmine.createSpyObj('PizzaService', [
      'getPizzaById',
      'createPizza',
      'updatePizza',
      'removePizza'
    ]);
    toppingsService = jasmine.createSpyObj('ToppingsService', ['getToppings']);
    dataSubject = new Subject();
    route = {
      ...new ActivatedRoute(),
      data: dataSubject.asObservable()
    } as ActivatedRoute;
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      declarations: [
        PizzaDetailComponent,
        MockComponents(PizzaFormComponent, PizzaDisplayComponent)
      ],
      providers: [
        {
          provide: PizzasService,
          useValue: pizzasService
        },
        {
          provide: ToppingsService,
          useValue: toppingsService
        },
        {
          provide: ActivatedRoute,
          useValue: route
        }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(PizzaDetailComponent);
    component = fixture.componentInstance;

    router = TestBed.inject(Router);

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('getCurrentPizza', () => {
    it('return undefined when pizza is not defined in data object', fakeAsync(() => {
      const pizza = null;
      const data = {};

      component.getCurrentPizza().subscribe({
        next: res => expect(res).toEqual(pizza),
        error: () => fail('failed to getCurrentPizza')
      });
      dataSubject.next(data);
      flush();
    }));

    it('return pizza when pizza is defined in data object', fakeAsync(() => {
      const pizza = pizzaFixture;
      const data = { pizza };

      component.getCurrentPizza().subscribe({
        next: res => expect(res).toEqual(pizza),
        error: () => fail('failed to getCurrentPizza')
      });
      dataSubject.next(data);
      flush();
    }));
  });

  describe('getToppings', () => {
    it('should retrieve topping list from toppingsService', () => {
      component.getToppings();

      expect(toppingsService.getToppings).toHaveBeenCalled();
    });
  });

  describe('getVisualisedToppings', () => {
    it('should return current pizza toppings', () => {
      const pizza = pizzaFixture;
      const selectedToppings = [{ ...toppingFixture, name: 'test-topping' }];
      const selectedToppings$ = hot('-a-', { a: selectedToppings });
      const pizza$ = hot('-b-', { b: pizza });
      const getVisualisedToppings = hot('-c-', { c: pizza.toppings });
      const getCurrentPizzaSpy = spyOn(component, 'getCurrentPizza');

      component.selectedToppings$ = selectedToppings$ as unknown as BehaviorSubject<Topping[]>;
      getCurrentPizzaSpy.and.returnValue(pizza$);

      expect(component.getVisualisedToppings()).toBeObservable(getVisualisedToppings);
    });

    it('should return current pizza toppings first and selectedUserToppings second', () => {
      const pizza = pizzaFixture;
      const selectedToppings = [{ ...toppingFixture, name: 'test-topping' }];
      const selectedToppings$ = hot('-a-b-', { a: toppingFixture, b: selectedToppings });
      const pizza$ = hot('-c---', { c: pizza });
      const getVisualisedToppings = hot('-d-e-', { d: pizza.toppings, e: selectedToppings });
      const getCurrentPizzaSpy = spyOn(component, 'getCurrentPizza');

      component.selectedToppings$ = selectedToppings$ as unknown as BehaviorSubject<Topping[]>;
      getCurrentPizzaSpy.and.returnValue(pizza$);

      expect(component.getVisualisedToppings()).toBeObservable(getVisualisedToppings);
    });

    it('should return an empty array of toppings if no pizza is available', () => {
      const pizza = null;
      const selectedToppings = [{ ...toppingFixture, name: 'test-topping' }];
      const selectedToppings$ = hot('-a-', { a: selectedToppings });
      const pizza$ = hot('-b-', { b: pizza });
      const getVisualisedToppings = hot('-c-', { c: [] });
      const getCurrentPizzaSpy = spyOn(component, 'getCurrentPizza');

      component.selectedToppings$ = selectedToppings$ as unknown as BehaviorSubject<Topping[]>;
      getCurrentPizzaSpy.and.returnValue(pizza$);

      expect(component.getVisualisedToppings()).toBeObservable(getVisualisedToppings);
    });
  });

  describe('onSelect', () => {
    it('should push new value to selectedToppings$', () => {
      const toppings = [toppingFixture];
      const nextSpy = spyOn(component.selectedToppings$, 'next');

      component.onSelect(toppings);

      expect(nextSpy).toHaveBeenCalledWith(toppings);
    });
  });

  describe('onCreate', () => {
    it('should navigate to the update form', () => {
      const navigateSpy = spyOn(router, 'navigate');
      const newPizza = newPizzaFixture;
      const pizza = pizzaFixture;

      pizzasService.createPizza.and.returnValue(of(pizza));
      component.onCreate(newPizza);

      expect(navigateSpy).toHaveBeenCalledWith([`/pizzas/${pizza.id}`]);
      expect(pizzasService.createPizza).toHaveBeenCalledWith(newPizza);
    });
  });

  describe('onUpdate', () => {
    it('should navigate back to pizza list', () => {
      const navigateSpy = spyOn(router, 'navigate');
      const pizza = pizzaFixture;

      pizzasService.updatePizza.and.returnValue(of(pizza));
      component.onUpdate(pizza);

      expect(navigateSpy).toHaveBeenCalledWith([`/pizzas`]);
      expect(pizzasService.updatePizza).toHaveBeenCalledWith(pizza);
    });
  });

  describe('onRemove', () => {
    it('should navigate to the delte confirmation dialog', () => {
      const navigateSpy = spyOn(router, 'navigate');

      component.onRemove();

      expect(navigateSpy).toHaveBeenCalledWith(['delete'], { relativeTo: route });
    });
  });
});
