import { Pizza } from './pizza.model';

export type NewPizza = Omit<Pizza, 'id'>;
